<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>

	<?php echo Yii::app()->bootstrap->init();?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<![endif]-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">
	<!-- Start WOWSlider.com HEAD section -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/slider/engine1/style.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/slider/engine1/jquery.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- End WOWSlider.com HEAD section -->
	<title>SabThok.com</title>

</head>

<!-- page -->
<body>
	<div class="nav">
		<section id="navigation-main">   
			<?php require_once('tpl_navigation.php') ?>
		</section>
	</div>
	<div class="container" id="page">
	
	<!-- breadcrumbs -->
	<?php if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
				'links'=>$this->breadcrumbs,
			)); ?>
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear">
		
	</div>
	<!-- footer -->
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by SabThok <br/>
		All Rights Reserved.<br/>
	</div>

</div>

</body>
</html>
