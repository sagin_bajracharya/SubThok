<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
    <div class="container">
        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
     
          <!-- Be sure to leave the brand out there if you want it shown -->
          <a class="brand" href="index" align="left"><img style="width: 20px; height: 20px; margin-top: -3px; border: solid;" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo/Logo1.png">  SabThok.com</a>
          
          <div class="nav-collapse">
			      <?php $this->widget('zii.widgets.CMenu',array(
                    'htmlOptions'=>array('class'=>'pull-right nav'),
                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
					          'itemCssClass'=>'item-test',
                    'encodeLabel'=>false,
                    'items'=>array(
                        array('label'=>'Home & Decor', 'url'=>array('/site/index')),
                        array('label'=>'Products <span class="caret"></span>', 'url'=>'#','itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"), 
                            'items'=>array(
                                array('label'=>'Men Store','url'=>array('/site/page?view=menStore')),
                                array('label'=>'Women Store','url'=>array('/site/page?view=womenStore')),
                                array('label'=>'Children Store','url'=>array('/site/page?view=childrenStore')),
                        )),
                        array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
                    ),
            )); ?>
    	</div>
    </div>
	</div>
</div>
